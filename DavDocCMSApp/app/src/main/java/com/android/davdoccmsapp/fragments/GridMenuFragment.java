package com.android.davdoccmsapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.davdoccmsapp.BooksActivity;
import com.android.davdoccmsapp.CalendarActivity;
import com.android.davdoccmsapp.CoursesActivity;
import com.android.davdoccmsapp.NotesActivity;
import com.android.davdoccmsapp.ProfileActivity;
import com.android.davdoccmsapp.R;
import com.android.davdoccmsapp.adapters.GridMenuAdapter;
import com.android.davdoccmsapp.model.MenuModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GridMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GridMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GridMenuFragment extends Fragment implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener{

    private OnFragmentInteractionListener mListener;

    private GridMenuAdapter gridMenuAdapter;

    private static ArrayList<MenuModel> menuList;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param menuList menu listing for dashboard menu
     * @return A new instance of fragment GridMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GridMenuFragment newInstance(ArrayList<MenuModel> menuList) {
        GridMenuFragment.menuList = menuList;
        return new GridMenuFragment();
    }

    public GridMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grid_menu, container, false);

        GridView menuGridView = (GridView) view.findViewById(R.id.contentContainer);
        gridMenuAdapter = new GridMenuAdapter(getActivity(), menuList);
        menuGridView.setAdapter(gridMenuAdapter);
        menuGridView.setOnItemLongClickListener(this);
        menuGridView.setOnItemClickListener(this);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity().getBaseContext(), "" + gridMenuAdapter.getItem(position).menuTitle, Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (gridMenuAdapter.getItem(position).menuSlug){
            case "calendar":
                Intent calendarIntent = new Intent(getActivity(), CalendarActivity.class);
                startActivity(calendarIntent);
                break;
            case "books":
                Intent booksIntent = new Intent(getActivity(), BooksActivity.class);
                startActivity(booksIntent);
                break;
            case "profile":
                Intent profileIntent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(profileIntent);
                break;
            case "notes":
                Intent notesIntent = new Intent(getActivity(), NotesActivity.class);
                startActivity(notesIntent);
                break;
            case "courses":
                Intent coursesIntent = new Intent(getActivity(), CoursesActivity.class);
                startActivity(coursesIntent);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
