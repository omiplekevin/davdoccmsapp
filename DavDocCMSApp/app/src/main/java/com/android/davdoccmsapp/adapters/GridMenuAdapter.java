package com.android.davdoccmsapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.android.davdoccmsapp.R;
import com.android.davdoccmsapp.model.MenuModel;

import java.util.ArrayList;

/**
 * Created by OMIPLEKEVIN on 28/09/2015.
 */
public class GridMenuAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<MenuModel> mItems;

    public GridMenuAdapter(Context context, ArrayList<MenuModel> items){
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public MenuModel getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.grid_menu_item, null);
            holder.menuIcon = (ImageView) convertView.findViewById(R.id.menuIcon);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.menuIcon.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), mItems.get(position).menuDrawable));
        holder.menuTitle = mItems.get(position).menuTitle;
        holder.menuSlug = mItems.get(position).menuSlug;

        return convertView;
    }

    class ViewHolder{
        ImageView menuIcon;
        String menuTitle;
        String menuSlug;
    }
}
