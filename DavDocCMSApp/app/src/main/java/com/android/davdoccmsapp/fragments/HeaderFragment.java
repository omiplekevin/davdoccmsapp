package com.android.davdoccmsapp.fragments;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.davdoccmsapp.R;

/**
 * Created by OMIPLEKEVIN on 29/09/2015.
 */
public class HeaderFragment extends Fragment {

    /**
     * module icon argument field name
     */
    public static final String ARG1 = "MODULE_ICON";

    /**
     * module name argument field name
     */
    public static final String ARG2 = "MODULE_NAME";

    /**
     * creates new instance of the fragment
     * @param moduleIcon module icon used by the header
     * @param moduleName module name to be indicated on the header
     * @return instance of HeaderFragment
     */
    public static HeaderFragment newInstance(int moduleIcon, String moduleName){
        HeaderFragment headerFragment = new HeaderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG1, moduleIcon);
        args.putString(ARG2, moduleName);
        headerFragment.setArguments(args);
        return headerFragment;
    }

    public HeaderFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_default_header, container, false);
        ImageView headerIcon = (ImageView) view.findViewById(R.id.headerIcon);
        TextView headerTitle = (TextView) view.findViewById(R.id.headerTitle);

        headerIcon.setImageBitmap(BitmapFactory.decodeResource(getResources(), getArguments().getInt(ARG1)));
        headerIcon.invalidate();

        headerTitle.setText(getArguments().getString(ARG2));
        headerTitle.invalidate();

        return view;
    }
}
