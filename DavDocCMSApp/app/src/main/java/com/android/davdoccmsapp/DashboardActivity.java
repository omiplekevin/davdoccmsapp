package com.android.davdoccmsapp;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import com.android.davdoccmsapp.fragments.GridMenuFragment;
import com.android.davdoccmsapp.fragments.HeaderFragment;
import com.android.davdoccmsapp.model.MenuModel;

import java.util.ArrayList;

public class DashboardActivity extends FragmentActivity implements GridMenuFragment.OnFragmentInteractionListener{

    public static ArrayList<MenuModel> mMenuList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // TODO: 27/09/2015 replace parameters with icon list need for the menu placement
        GridMenuFragment gridMenuFragment = GridMenuFragment.newInstance(mMenuList);
        HeaderFragment headerFragment = HeaderFragment.newInstance(R.drawable.davdoc_logo, "Davao Doctor College CMS");

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, gridMenuFragment);
        fragmentTransaction.commit();

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.header, headerFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_splash_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
