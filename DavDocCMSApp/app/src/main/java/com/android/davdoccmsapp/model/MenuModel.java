package com.android.davdoccmsapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by OMIPLEKEVIN on 30/09/2015.
 */
public class MenuModel implements Serializable{

    @SerializedName("menuTitle")
    public String menuTitle;

    @SerializedName("menuDrawable")
    public int menuDrawable;

    @SerializedName("menuSlug")
    public String menuSlug;
}
